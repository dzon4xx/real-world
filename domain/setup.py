from distutils.core import setup

from setuptools import find_packages


setup(
    name='domain',
    version='0.1',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'pytest==4.3.1'
    ],
    url='',
    license='',
    author='dzon4xx',
    author_email='',
    description='Holds clean domain of real world application'
)
